So its a fairly simple approach towards the problem statement

1. **Get 2000 wedding images** - I used `import.io` for getting data from `https://pixabay.com/en/photos/wedding/?&pagi=1`. Although I got total 4000 image urls I trimmed down to 2000.

2. **Getting tags for the images from Clarifai** - Installed their python package and made a small program which you can find in clarifai folder. There is a `ClarifaiService` which has methods which basically read data from csv and predict the tags of images and store them into ElasticSearch for which I have already built and index with a sample schema.


3. **Built a rest api to the elastic**- An application `elastic_search` where `localhost:8000/search_tags?keyword=KEYWORD TO BE FOUND IN TAGS` and it will return all the matching entities with partial matching based on the query I made in the `elastic_search -> query`

4. **Screenshots** - In screenshot folder

5. **Data from Clarifai** - artifacia_images_data_tags.csv(You can check the data which is being returned for particular tag) 

So Basically my database here is `elastic search` through which I have indexed all the `urls` of `images` with their marked `tags` from `clarifai` and written a `elastic search` simple `query` to search for the `tags` and return the matched `documents`.