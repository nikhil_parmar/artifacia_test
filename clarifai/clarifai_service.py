from clarifai.rest import (
    ClarifaiApp
)
import csv
from elasticsearch import (
    Elasticsearch,
)
__all__ = [
    "clarifai_obj"
]


class ClarifaiService(object):
    def __init__(self):
        app = ClarifaiApp()
        self.model = app.models.get('general-v1.3')
        self.es_obj = Elasticsearch()

    def read_urls_from_csv(self, csv_name):
        urls_list = []
        with open(csv_name, 'r') as csv_file:
            reader = csv.reader(csv_file)
            for row in reader:
                urls_list.append(row[0])
        return urls_list

    def predict_url_tags(self, url):
        response = self.model.predict_by_url(url)
        return response

    def insert_data_elastic_search(self, data, url):
        tags = []
        if len(data['concepts']) > 0:
            for item in data['concepts']:
                tags.append(item['name'])
            data = {"url": url, "tags": tags}
            self.es_obj.index(index="image_data", doc_type="urls_tag",
                              body=data)
        return True

    def process_urls(self, csv_name):
        urls_data = self.read_urls_from_csv(csv_name)
        for url in urls_data:
            predict_tags = self.predict_url_tags(url)
            print predict_tags
            try:

                push_data = self.insert_data_elastic_search(predict_tags['outputs'][0]
                                                      ['data'], url)
            except:
                pass
        return True

clarifai_obj = ClarifaiService()
clarifai_obj.process_urls("wedding_urls.csv")








