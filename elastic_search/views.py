from rest_framework.views import (
    APIView
)
from rest_framework.response import (
    Response
)
from rest_framework.status import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_201_CREATED,
    HTTP_200_OK
)
from elastic_search_service import (
    ElasticSearchQuery
)
from artifacia_test.settings import (
    ES_DOC_TYPE,
    ES_INDEX_NAME
)
from query import (
    get_query
)
from utility import (
    _response_serializer_rest
)

__all__ = [
    "GetSimilarItems"
]


class GetSimilarItems(APIView):

    def get(self, request):
        index_name = ES_INDEX_NAME
        doc_type = ES_DOC_TYPE
        keyword = request.query_params['keyword']
        query = get_query(keyword)
        obj = ElasticSearchQuery()
        response = obj.get_similar_items(index_name, doc_type, **query)
        serialize_data = _response_serializer_rest(response)
        return Response({'message': serialize_data}, status=HTTP_200_OK)