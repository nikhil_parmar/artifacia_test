__all__ = [
    'get_query'
]


def get_query(keyword):
    _query = {"query":
                    {"bool":
                         {"must":
                              [
                                  {"match_phrase_prefix":
                                    {
                                        "tags": keyword
                                     }
                                }
                               ],
                          "must_not":
                              [],
                          "should":[]
                          }
                     },
                "from": 0, "size": 10, "sort": [], "aggs": {}
                }
    return _query
