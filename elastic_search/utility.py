__all__ = [
    "_response_serializer_rest",
]


def _response_serializer_rest(response):
    _data_list = []

    data = response['hits']
    for item in data['hits']:
        _data = {"url": item['_source']['url']}
        _data_list.append(_data)
    return _data_list
